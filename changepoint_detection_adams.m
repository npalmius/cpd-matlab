function [ posterior  ] = changepoint_detection_adams( x )
% Copyright (c) 2014, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 23-Aug-2014

lambda = 150;

mu_prior = 12.5;
sigma_prior = 0.1;

nu_prior = 1;
chi_prior = [mu_prior ((mu_prior^2) + sigma_prior)];
%chi_prior = [(mu_prior/(sigma_prior^2)) (-1/(2 * (sigma_prior ^ 2)))];
%eta_prior = [mu_prior, ((mu_prior ^ 2) + (sigma_prior ^ 2))];

p_runs = cell(length(x), 1);
nu = zeros(length(x), 1);
chi = zeros(length(x), 2);
%eta = zeros(length(x), 2);
%pi = zeros(length(x), 1);

p_runs{1} = 1;
nu(1) = nu_prior;
chi(1, :) = chi_prior;
%eta(1, :) = eta_prior;

%mu_grid = zeros(length(x), length(x));
%std_grid = zeros(length(x), length(x));

x_predicted_mu = NaN(length(x) + 1, 1);
x_predicted_sigma_squared = NaN(length(x) + 1, 1);

E_run_length = NaN(length(x) + 1, 1);

H = 1 / lambda;

for t = 1:(length(x) - 1)
    run_start_p = 0;
    run_start_i = t + 1;
    for run_start = 1:t
        run_length = (t - run_start + 1);
        if length(p_runs{run_start}) == (run_length);
            run_start_i = [run_start_i run_start];
            mu = chi(run_start, 1) / nu(run_start);
            sigma_squared = (chi(run_start, 2) / nu(run_start)) - (mu ^ 2);
            
            pi = normpdf(x(t), mu, sqrt(sigma_squared));
            
            p_run_cont = p_runs{run_start}(run_length) * pi * (1-H);
            p_run_new =  p_runs{run_start}(run_length) * pi * H;

            run_start_p = [run_start_p p_run_cont];
            run_start_p(1) = run_start_p(1) + p_run_new;

            nu(run_start) = nu(run_start) + 1;
            %mu_grid(t,r) = mu;
            %std_grid(t,r) = sqrt(sigma_squared);
            chi(run_start, :) = chi(run_start, :) + [x(t) x(t)^2];
            %eta(r, :) = eta(r, :) + [x(t) x(t)^2];
        end
    end
    run_start_p = run_start_p / sum(run_start_p);
    run_start_p(find(run_start_p < 10 ^ -4)) = 0;
    run_start_p = run_start_p / sum(run_start_p);
    nu(t + 1) = nu_prior;
    chi(t + 1, :) = chi_prior;
    
    % Predict moments, from http://en.wikipedia.org/wiki/Mixture_distribution#Moments
    x_predicted_mu_t = zeros(size(run_start_p));
    x_predicted_sigma_squared_t = zeros(size(run_start_p));
    
    for I = find(run_start_p > 0)
        p_runs{run_start_i(I)} = [p_runs{run_start_i(I)} run_start_p(I)];
        
        x_predicted_mu_t(I) = chi(run_start_i(I), 1) / nu(run_start_i(I));
        x_predicted_sigma_squared_t(I) = (chi(run_start_i(I), 2) / nu(run_start_i(I))) - (x_predicted_mu_t(I) ^ 2);
    end
    
    x_predicted_mu(t + 1) = sum(run_start_p .* x_predicted_mu_t);
    x_predicted_sigma_squared(t + 1) = sum(run_start_p .* (((x_predicted_mu_t - x_predicted_mu(t + 1)) .^ 2) + x_predicted_sigma_squared_t));

    % Compute expected run length
    E_run_length(t + 1) = sum( (t - run_start_i + 2) .* run_start_p );
    
    %chi(t + 1, :) = [x(t) (x(t)^2 + 0.1)]; %chi_prior;
    %eta(t + 1, :) = eta_prior;
    
end

max_run_length = max(nu) - 1;

p = zeros(length(x), ceil(max_run_length / 100) * 100);

for t = 1:(length(x))
    for r = 1:(nu(t) -1)
        p(t + r - 1, r) = p_runs{t}(r);
    end
end

posterior = p';

figure;
imagesc(posterior);
set(gca,'YDir','normal')
colorbar

colormap([1 1 1; flipud(colormap('jet'))]);

%fig2plotly(gcf, 'name', 'changepoint-detection-adams-posterior')

figure

plot(x);
hold on
plot(x_predicted_mu, 'r', 'LineWidth', 3);
plot(x_predicted_mu + x_predicted_sigma_squared, 'color', [0.5 0.5 0.5], 'LineWidth', 2);
plot(x_predicted_mu - x_predicted_sigma_squared, 'color', [0.5 0.5 0.5], 'LineWidth', 2);
hold off

%fig2plotly(gcf, 'name', 'changepoint-detection-adams-predictive')

figure
plot(E_run_length);

%figure;
%imagesc(mu_grid);
%colorbar

%figure;
%imagesc(std_grid);
%colorbar

end

